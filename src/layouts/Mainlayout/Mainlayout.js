import styles from "./Mainlayout.module.scss";
import { NotificationsNoneOutlined } from "@mui/icons-material";
import MenuProfile from "../../components//MenuCustom/MenuProfile";
import { Link } from "react-router-dom";

export default function Mainlayout({ title, content }) {
    return (
        <div className={styles.mainlayout}>
            <div className={styles.header}>
                <div className={styles.header_left}>
                    <div className={styles.menu}>
                        <Link to="/user/home">Trang chủ</Link>
                        <Link to="/user/activity">
                            Các hoạt động đã đăng ký
                        </Link>
                    </div>
                </div>
                <div className={styles.header_right}>
                    <div>
                        <NotificationsNoneOutlined />
                    </div>

                    <MenuProfile />
                </div>
            </div>

            <div className={styles.container}>
                <div className={styles.title_content}>
                    <div className={styles.title}>{title}</div>
                    <div className={styles.content}>{content}</div>
                </div>
            </div>
        </div>
    );
}
