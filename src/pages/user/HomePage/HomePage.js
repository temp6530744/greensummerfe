import React, { useCallback, useEffect, useState } from "react";
import MainLayout from "../../../layouts/Mainlayout/Mainlayout";
import Activity from "../../../components/Activity/Activity";
import { Box } from "@mui/material";
import { useNavigate } from "react-router-dom";
import TimeVN from "../../../untils/timeVN";
import {
    getAllProjects,
    createApplication,
    getApplicationRegisted,
} from "../../../api";
import { jwtDecode } from "jwt-decode";
import { toast } from "react-toastify";

const Child = () => {
    const navigate = useNavigate();

    const [mssv, setMssv] = useState("");
    const [listProject, setListProject] = useState([]);
    const [listID, setListID] = useState([]);

    const handleGetApplication = useCallback(async () => {
        const token = localStorage.getItem("JWT");
        const json = jwtDecode(token);

        if (json.mssv) {
            const response = await getApplicationRegisted(json.mssv, token);
            if (response?.status === 200) {
                response?.data.metadata.data?.map((item) =>
                    setListID((prev) => [...prev, item?.project_id])
                );
            }
        }
    }, []);

    useEffect(() => {
        handleGetApplication();
    }, [handleGetApplication]);

    const loadListProject = useCallback(async () => {
        const res = await getAllProjects();
        if (res?.status === 200) {
            const filteredData = res?.data.metadata.data.filter(
                (item) =>
                    item.status === "Được xét duyệt" &&
                    !listID?.includes(item.project_id)
            );
            setListProject(filteredData);
        }
    }, [listID]);
    useEffect(() => {
        loadListProject();
    }, [listID, loadListProject]);

    const handleClickDetail = (id) => {
        navigate(`/user/home/detail?id=${id}`);
    };

    useEffect(() => {
        const token = localStorage.getItem("JWT");
        if (token) {
            const json = jwtDecode(token);
            setMssv(json.mssv);
        }
    }, []);

    const handleRegisterActivity = async (project_id) => {
        const token = localStorage.getItem("JWT");
        try {
            const res = await createApplication(
                {
                    mssv,
                    project_id,
                },
                token
            );
            if (res.data.message === "Create application successfully!") {
                toast.success("Đăng ký hoạt động thành công");
                window.location.reload();
            } else {
                toast.error("Không thể đăng ký hoạt động này");
            }
        } catch (err) {
            console.log("err:", err);
        }
    };

    return (
        <Box sx={{ display: "flex", flexDirection: "column", gap: "15px" }}>
            {listProject &&
                listProject?.map((index) => (
                    <Activity
                        title={index.title}
                        content={index.content}
                        time={TimeVN(index.createdAt)}
                        place={index.location}
                        handleClickDetail={() =>
                            handleClickDetail(index.project_id)
                        }
                        handleRegisterActivity={() =>
                            handleRegisterActivity(index.project_id)
                        }
                        // project_id={index.project_id}
                        status="all"
                    />
                ))}
        </Box>
    );
};

export default function HomePage() {
    return (
        <div>
            <MainLayout title="Tất cả các hoạt động" content={<Child />} />
        </div>
    );
}
