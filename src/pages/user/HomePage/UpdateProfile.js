import React, { useState, useEffect } from "react";
import MainLayout from "../../../layouts/Mainlayout/Mainlayout";
import Input from "../../../components/Input/Input";
import { Box, Button } from "@mui/material";
import Dropdown from "../../../components/Dropdown/Dropdown";
import { getProfile, updateProfile } from "../../../api";
import { toast } from "react-toastify";
import { jwtDecode } from "jwt-decode";
import { useNavigate } from "react-router-dom";

const Child = () => {
  const navigate = useNavigate();
  const [data, setData] = useState({
    fullname: "",
    username: "",
    mssv: "",
    gmail: "",
    school: "",
  });
  const loadUserProfile = async () => {
    const token = localStorage.getItem("JWT");
    if (token) {
      const json = jwtDecode(token);

      if (json.mssv) {
        const res = await getProfile(json.mssv, token);
        setData(res.data.data);
      } else {
        toast.error("Phiên đăng nhập đã hết hạn");
        navigate("/user/login");
      }
    }
  };
  useEffect(() => {
    loadUserProfile();
  }, []);

  const handleChangeInput = (event) => {
    const { name, value } = event.target;
    setData({
      ...data,
      [name]: value,
    });
  };

  const handleUpdateProfile = async () => {
    const token = localStorage.getItem("JWT");

    const originalData = { ...data };

    try {
      const json = jwtDecode(token);

      const res = await updateProfile(
        {
          fullname: data.fullname,
          username: data.username,
          gmail: data.gmail,
          school: data.school,
        },
        json.mssv,
        token
      );

      if (res) {
        setData(res.data.data);
        toast.success("Cập nhật thông tin thành công");
      }
    } catch (err) {
      console.log("error:", err);
      console.log("data:", data);
      toast.error("Không thể cập nhật thông tin");
      setData(originalData);
    }

    navigate("/user/profile");
  };

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        gap: "15px",
      }}
    >
      <>
        {" "}
        <Box sx={{ display: "flex", gap: "20px" }}>
          <Input
            label="Họ và tên"
            name="fullname"
            value={data.fullname || ""}
            onChange={handleChangeInput}
          />
          <Input
            label="Username"
            name="username"
            value={data.username}
            onChange={handleChangeInput}
          />
        </Box>
        <Box sx={{ display: "flex", gap: "20px" }}>
          <Input
            label="Mã số sinh viên"
            name="mssv"
            value={data.mssv}
            disabled={true}
            onChange={handleChangeInput}
          />
          <Input
            label="Email"
            onChange={handleChangeInput}
            name="gmail"
            value={data.gmail}
          />
        </Box>
        <Box sx={{ display: "flex", gap: "20px" }}>
          <Dropdown
            label="Trường Đại học"
            listItem={[
              { id: 1, name: "Đại học Bách Khoa TPHCM" },
              { id: 2, name: "Đại học Công nghệ thông tin" },
              { id: 3, name: "Đại học Khoa học Tự nhiên" },
            ]}
            name="school"
            value={data.school}
            onChange={handleChangeInput}
          />
        </Box>
      </>

      <Box sx={{ display: "flex", gap: "20px" }}>
        <Box sx={{ minWidth: "300px" }}></Box>
        <Box
          sx={{
            display: "flex",
            justifyContent: "flex-end",
            gap: "20px",
            minWidth: "300px",
          }}
        >
          <>
            <Button
              variant="contained"
              size="medium"
              color="error"
              sx={{ textTransform: "none" }}
              onClick={() => navigate("/user/profile")}
            >
              Hủy
            </Button>
            <Button
              variant="contained"
              size="medium"
              color="primary"
              sx={{ textTransform: "none" }}
              onClick={handleUpdateProfile}
            >
              Cập nhật
            </Button>
          </>
        </Box>
      </Box>
    </Box>
  );
};

export default function Profile() {
  return (
    <div>
      <MainLayout title="Tài khoản của tôi" content={<Child />} />
    </div>
  );
}
