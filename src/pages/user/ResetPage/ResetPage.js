import React, { useState, useEffect } from "react";
import AuthenticationLayout from "../../../layouts/Authentication/Authentication";
import { toast } from "react-toastify";
import { useNavigate, useParams } from "react-router-dom";
import { Box, Button } from "@mui/material";
import Input from "../../../components/Input/Input";
import { getAllStudents, resetPassword } from "../../../api";

const Child = () => {
  const [email, setEmail] = useState("");
  const [tokenReset, setTokenReset] = useState("");
  const [password, setPassword] = useState("");

  useEffect(() => {
    const urlParams = new URLSearchParams(window.location.search);
    const email = urlParams.get("email");
    setEmail(email);
  }, []);
  const [data, setData] = useState([]);
  const navigate = useNavigate();

  const loadAllStudents = async () => {
    try {
      const res = await getAllStudents();
      setData(res.data.metadata.data);
    } catch (err) {}
  };

  function getMssvByEmail(email) {
    const user = data.find((user) => user.gmail === email);
    return user ? user.mssv : null;
  }

  const MSSV = getMssvByEmail(email);

  useEffect(() => {
    loadAllStudents();
  }, []);

  const handleConfirm = async () => {
    try {
      const res = await resetPassword(
        { password: password, mssv: MSSV },
        tokenReset
      );
      console.log(res);
      if (res.data.status === "Success") {
        toast.success("Thiết lập mật khẩu thành công");
        navigate("/user/login");
      } else {
        toast.error("Mã reset không chính xác");
      }
    } catch (err) {
      toast.error("Không thể thiết lập mật khẩu");
    }
  };

  return (
    <Box sx={{ display: "flex", flexDirection: "column", gap: "20px" }}>
      <Box sx={{ fontSize: "16px", fontWeight: "bold" }}>
        Thiết lập lại mật khẩu
      </Box>
      <Input label="Email Reset" value={email} disabled={true} />
      <Input label="Mã số sinh viên" value={MSSV} disabled={true} />
      <Input
        label="Mã reset mật khẩu nhận được từ email"
        value={tokenReset}
        name="tokenReset"
        onChange={(e) => setTokenReset(e.target.value)}
      />
      <Input
        label="Nhập mật khẩu mới"
        value={password}
        name="password"
        onChange={(e) => setPassword(e.target.value)}
        type="password"
      />
      <Button variant="contained" onClick={handleConfirm}>
        Xác nhận
      </Button>
    </Box>
  );
};
export default function ResetPage() {
  return <AuthenticationLayout children={<Child />} />;
}
