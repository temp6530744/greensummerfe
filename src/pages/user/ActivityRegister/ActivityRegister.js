import { useCallback, useEffect, useState } from "react";
import Mainlayout from "../../../layouts/Mainlayout/Mainlayout";
import { getApplicationRegisted, getAllProjects } from "../../../api";
import { Box } from "@mui/material";
import Activity from "../../../components/Activity/Activity";
import { jwtDecode } from "jwt-decode";
import TimeVN from "../../../untils/timeVN";
import { useNavigate } from "react-router-dom";

const Child = () => {
  const navigate = useNavigate();
  const [dataTable, setDataTable] = useState([]);
  const [allProject, setAllProject] = useState([]);

  function mergeData(dataTable, allProject) {
    return dataTable.map((data) => {
      const projectInfo = allProject.find(
        (project) => project.project_id === data.project_id
      );
      if (projectInfo) {
        return { ...projectInfo, status: data.status };
      }
      return data;
    });
  }

  const handleGetApplication = useCallback(async () => {
    const token = localStorage.getItem("JWT");
    const json = jwtDecode(token);

    if (json.mssv) {
      const response = await getApplicationRegisted(json.mssv, token);
      console.log(response);
      if (response?.status === 200) {
        const mergedData = mergeData(response?.data.metadata.data, allProject);
        setDataTable(mergedData);
      }
    }
  }, [allProject]);

  useEffect(() => {
    const loadListProject = async () => {
      const res = await getAllProjects();
      if (res?.status === 200) {
        setAllProject(res?.data.metadata.data);
      }
    };

    loadListProject();
  }, []);

  useEffect(() => {
    handleGetApplication();
  }, [handleGetApplication]);

  const handleClickDetail = (id) => {
    navigate(`/user/home/detail?id=${id}`);
  };

  return (
    <Box sx={{ display: "flex", flexDirection: "column", gap: "15px" }}>
      {dataTable &&
        dataTable?.map((index) => (
          <Activity
            title={index.title}
            content={index.content}
            time={TimeVN(index.createdAt)}
            place={index.location}
            handleClickDetail={() => handleClickDetail(index.project_id)}
            status={
              index.status === true
                ? "Đã xác nhận"
                : index.status === false
                ? "Đã từ chối"
                : "Chờ phê duyệt"
            }
            disabledStatus={true}
          />
        ))}
    </Box>
  );
};

export default function ActivityRegister() {
  return (
    <div>
      <Mainlayout title="Các hoạt động đã đăng ký" content={<Child />} />
    </div>
  );
}
