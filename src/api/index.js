import axios from "axios";

export const getProfile = async (mssv, token) => {
    return await axios.get(`http://localhost:5000/v1/api/student/${mssv}`, {
        headers: {
            Authorization: `${token}`,
        },
    });
};

export const updateProfile = async (data, mssv, token) => {
    return await axios({
        method: "patch",
        data: data,
        url: `http://localhost:5000/v1/api/student/${mssv}`,
        headers: {
            Authorization: `${token}`,
        },
    });
};

export const createAcc = async (data) => {
    return await axios({
        method: "post",
        data: data,
        url: `http://localhost:5000/v1/api/user/register`,
    });
};

export const loginPost = async (data) => {
    return await axios({
        method: "post",
        data: data,
        url: `http://localhost:5000/v1/api/user/login`,
    });
};

export const forgotPassword = async (data) => {
    return await axios({
        method: "post",
        data: data,
        url: `http://localhost:5000/v1/api/user/forgotpassword`,
    });
};

export const resetPassword = async (data, token) => {
    return await axios({
        method: "post",
        data: data,
        url: `http://localhost:5000/v1/api/user/resetpassword`,
        headers: {
            Authorization: `${token}`,
        },
    });
};

export const createApplication = async (data, token) => {
    return await axios({
        method: "post",
        data: data,
        url: `http://localhost:5000/v1/api/student/apply`,
        headers: {
            Authorization: `${token}`,
        },
    });
};

export const update = async (data) => {
    return await axios({
        method: "put",
        data: data,
        url: `...`,
        headers: {
            Authorization: `Bearer ...`,
        },
    });
};

export const deleted = async (data) => {
    return await axios({
        method: "delete",
        url: `...${data}`,
        headers: {
            Authorization: `Bearer ...`,
        },
    });
};

export const createProject = async (data) => {
    return await axios({
        method: "post",
        data: data,
        url: `http://localhost:5000/v1/api/project`,
    });
};

export const getAllProjects = async () => {
    return await axios.get(`http://localhost:5000/v1/api/project/all`, {
        headers: {
            Authorization: `Bearer ...`,
        },
    });
};

export const getAllStudents = async () => {
    return await axios.get(`http://localhost:5000/v1/api/student/all`, {
        headers: {
            Authorization: `Bearer ...`,
        },
    });
};

export const getOneProject = async (id) => {
    return await axios.get(`http://localhost:5000/v1/api/project/${id}`, {
        headers: {
            Authorization: `Bearer ...`,
        },
    });
};

export const getAllApplications = async () => {
    return await axios.get(`http://localhost:5000/v1/api/application/all`, {
        headers: {
            Authorization: `Bearer ...`,
        },
    });
};

export const getStudentById = async (id, token) => {
    return await axios.get(`http://localhost:5000/v1/api/student/${id}`, {
        headers: {
            Authorization: `${token}`,
        },
    });
};

export const getActById = async (id) => {
    return await axios.get(`http://localhost:5000/v1/api/project/${id}`, {
        headers: {
            Authorization: `Bearer ...`,
        },
    });
};

export const approveApplication = async (id, data, token) => {
    return await axios({
        method: "patch",
        data: data,
        url: `http://localhost:5000/v1/api/application/${id}`,
        headers: {
            Authorization: `${token}`,
        },
    });
};

export const approveProject = async (id, data, token) => {
    return await axios({
        method: "patch",
        data: data,
        url: `http://localhost:5000/v1/api/project/verify/${id}`,
        headers: {
            Authorization: `${token}`,
        },
    });
};

export const getApplicationRegisted = async (id, token) => {
    return await axios.get(
        `http://localhost:5000/v1/api/student/application/${id}`,
        {
            headers: {
                Authorization: `${token}`,
            },
        }
    );
};
